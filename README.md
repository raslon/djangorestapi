**Создание виртуального окружения**
---
```virtualenv venv```

***Активация виртуального окружения***

Для активации виртуального окружения воспользуйтесь командой (для Linux):

```source venv/bin/activate```

Для Windows команда будет выглядеть так:

```venv\Scripts\activate```

___

**Установка зависимостей**
---
```pip install -r requirements.txt```
___
**Запуск**
---
```python manage.py runserver```
___