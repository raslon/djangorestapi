from django.shortcuts import render
from rest_framework import viewsets, permissions

from api.serializers import StudentSerializer
from student.models import Student


class StudentView(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]

    serializer_class = StudentSerializer