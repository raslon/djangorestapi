from rest_framework import routers
from .views import StudentView

router = routers.DefaultRouter()
router.register('api/student', StudentView, 'student')

urlpatterns = router.urls