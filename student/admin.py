from django.contrib import admin
from .models import Student
# Register your models here.


class StudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'surname', 'name', 'patronymic', 'date_of_birth', 'performanc')
    list_display_links = ('id', 'name')
    search_fields = ('id', 'surname', 'name', 'patronymic', 'performanc')
    list_filter = ('surname','name')


admin.site.register(Student, StudentAdmin)