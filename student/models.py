from django.db import models


# Create your models here.

class Student(models.Model):
    EXCELLENT_MARK = 'Отличник'
    GOOD_MARK = 'Хорошист'
    MEDIOCRE = 'Ударник'
    LOW_MARK = 'Неудовлетворительно'

    ACADEMIC_PERFORMANC = (
        (EXCELLENT_MARK, 'Отличник'),
        (GOOD_MARK, 'Хорошист'),
        (MEDIOCRE, 'Ударник'),
        (LOW_MARK, 'Неудовлетворительно'),
    )

    surname = models.CharField(max_length=50, null=False, verbose_name='Фамилия')
    name = models.CharField(max_length=50, null=False, verbose_name='Имя')
    patronymic = models.CharField(max_length=50, null=False, verbose_name='Отчество')
    date_of_birth = models.DateField(verbose_name='Дата рождения', null=False)
    performanc = models.CharField(max_length=20, choices=ACADEMIC_PERFORMANC, null=False, default='low_mark')

    def __str__(self):
        return '{} {} {}'.format(self.surname, self.name, self.patronymic)
